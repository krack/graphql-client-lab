# GraphQL lab - serveur

## Contexte

### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
`@Controller` spring web qui contiendra le code des `Query` et `Resolver` GraphQL qui sera ajouté au tag `/** ADD CODE HERE */`.

Contient les données pour nos cas de tests.

### **`src/main/resources/graphql/schema.graphqls`**
Shéma graphql du service exposé.

### **`src/main/java/com/soprasteria/graphqllab/model`**
Dossier contenant le modèle final. Nous n'aurons pas à y touché durant ce TP.


## Exercice 1 : Récupérer tous les objets
A partir du projet dans [Gitpod](https://gitpod.io/#https://gitlab.com/krack/graphql-lab/-/tree/0-base/src/main/java/com/soprasteria/graphqllab/FilmQuery.java).

Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms: [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
}
```

Tester avec la Query
```GraphQL
{
  getFilms{
    title
    releaseYear
  }
}
```


<details>
  <summary>Informations</summary>

 L'annontation `@QueryMapping` de `org.springframework.graphql.data.method.annotation` permet de définir une méthode de classe comme étant la réponse à une query. Le paramètre `name` de cette annotation permet de définir le nom de la query sans utilisé une convention de nommage.

</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java
import org.springframework.graphql.data.method.annotation.QueryMapping;

/** ADD CODE HERE */
@QueryMapping(name = "getFilms")
public Iterable<Film> getFilms() {
    return this.films;
}
```

[1-ex1](https://gitlab.com/krack/graphql-lab/-/tags/1-ex1)

</details>

## Exercice 2 : Filter les objets récupérer


Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
}

```

Tester avec la Query
```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux"){
    title
  }
}
```

<details>
  <summary>Informations</summary>

 L'annontation `@Argument` de `org.springframework.graphql.data.method.annotation` permet de définir un paramètre de méthode comme étant l'argument d'une query. Le paramètre `name` de cette annotation permet de définir le nom de l'argument .

</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java
import org.springframework.graphql.data.method.annotation.Argument;

/** ADD CODE HERE */
@QueryMapping(name = "getFilms")
public Iterable<Film> getFilms(@Argument("search") String search) {
      // search filter
      if (search != null) {
        return this.films.stream().filter((film) -> film.getTitle().toLowerCase().contains(search.toLowerCase()))
                .collect(Collectors.toList());
    }
    return this.films;
}
```

[2-ex2](https://gitlab.com/krack/graphql-lab/-/tags/2-ex2)

</details>



## Exercice 3 : Résoudre un champs 
Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
    "the note of the film"
    note(
        """
        Get not of specific not referer : ALLOCINE by default
        """
        reference: NoteReference
    ): Float
}

"""
recognized institution deliver note for the film.
"""
enum NoteReference {
"""
Allocine institution
"""
  ALLOCINE
"""
Sylvain institution
"""
  SYLVAIN
}

```

La notation se basera sur un modulo 5 de la taille du titre dans la référence ALLOCINE.

La notation se basera sur un modulo 5 de la taille du titre divisé par 2 dans la référence SYLVAIN.


Tester avec les Query
```GraphQL
{
  getFilms{
    title
    note
  }
}

```

```GraphQL
{
  getFilms{
    title
    note(reference: ALLOCINE)
  }
}

```

```GraphQL
{
  getFilms{
    title
    note(reference: SYLVAIN)
  }
}

```

```GraphQL
{
  getFilms{
    title
    noteSylvain: note(reference: SYLVAIN)
    noteAllocine: note(reference: ALLOCINE)
  }
}

```
<details>
  <summary>Informations</summary>

L'annontation `@SchemaMapping` de `org.springframework.graphql.data.method.annotation` permet de définir une méthode de classe comme étant résolver d'un field d'un objet. Le paramètre `field` de l'annontation défini le nom du field dans l'objet qui doit être résolue, le paramètre `typeName` de l'annontation défini dans quel type d'objet le field est définie.

On récupérer également l'objet parent de notre context d'appel en déclarant un paramètre du type du parent dans la méthode de résolution.

</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java
import org.springframework.graphql.data.method.annotation.SchemaMapping;

//film resolver
@SchemaMapping(typeName = "Film", field = "note")
public Float getNotationOfFilm(@Argument("reference") NoteReference note, Film film) {
    var noteVeryGood = film.getTitle().length();
    if (note == NoteReference.SYLVAIN) {
        noteVeryGood = noteVeryGood / 2;
    }
    return  Float.valueOf(noteVeryGood % 5);
}
```

[3-ex3](https://gitlab.com/krack/graphql-lab/-/tags/3-ex3)

</details>

## Exercice 4 : Résoudre un objet lié


Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
    "the note of the film"
    note(
        """
        Get not of specific not referer : ALLOCINE by default
        """
        reference: NoteReference
    ): Float
    """
    The only one director of the film
    """
    director: Director!
}
"""
The Director of films
"""
type Director {
    """
    The name of director
    """
    name: String!
}

"""
recognized institution deliver note for the film.
"""
enum NoteReference {
"""
Allocine institution
"""
  ALLOCINE
"""
Sylvain institution
"""
  SYLVAIN
}


```

Tester avec la Query
```GraphQL
{
  getFilms{
    title
    director{
      name
    }
  }
}

```

<details>
  <summary>Informations</summary>

Rien à ajouté.


</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java

import java.util.Optional;

@SchemaMapping(typeName = "Film", field = "director")
public Optional<Director> getDirectorOfFilm(Film film) {
    return this.directors.stream().filter((director) -> director.getId() == film.getDirectorId()).findFirst();
}
```

[4-ex4](https://gitlab.com/krack/graphql-lab/-/tags/4-ex4)

</details>


## Exercice 5 : Gestion de graph : résolution cyclique
Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
    "the note of the film"
    note(
        """
        Get not of specific not referer : ALLOCINE by default
        """
        reference: NoteReference
    ): Float
    """
    The only one director of the film
    """
    director: Director!
}
"""
The Director of films
"""
type Director {
    """
    The name of director
    """
    name: String!
    """
    The film of director in role of director.
    """
    films: [Film]!
}


"""
recognized institution deliver note for the film.
"""
enum NoteReference {
"""
Allocine institution
"""
  ALLOCINE
"""
Sylvain institution
"""
  SYLVAIN
}


```

Tester avec la Query
```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux : Les Deux Tours"){
    director{
      films{
        title
        note(reference: SYLVAIN)
      }
    }
  }
}


```

<details>
  <summary>Informations</summary>

Rien à ajouté.


</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java

//director resolver

@SchemaMapping(typeName = "Director", field = "films")
public List<Film> getFilmsOfDirector(Director director) {
  return this.films.stream().filter((film) -> film.getDirectorId() == director.getId()).collect(Collectors.toList());
}
```

[5-ex5](https://gitlab.com/krack/graphql-lab/-/tags/5-ex5)

</details>


## Exercice 6 : Gestion de graph : filtrage de ressources liée
Réaliser un service qui répond à la Query décrite par le shéma suivant

#### **`src/main/resources/graphql/schema.graphqls`**
```GraphQL
# The Root Query for the application
type Query {
    """
    get all films.
    """
    getFilms(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}
"""
Film with fields represent him.
"""
type Film {
    "the title of the film"
    title: String!
    "the release year of first release of the film"
    releaseYear: Int!
    "the note of the film"
    note(
        """
        Get not of specific not referer : ALLOCINE by default
        """
        reference: NoteReference
    ): Float
    """
    The only one director of the film
    """
    director: Director!
}
"""
The Director of films
"""
type Director {
    """
    The name of director
    """
    name: String!
    """
    The film of director in role of director.
    """
    films(
        """
        Filter film contains the search term
        """
        search: String
    ): [Film]!
}


"""
recognized institution deliver note for the film.
"""
enum NoteReference {
"""
Allocine institution
"""
  ALLOCINE
"""
Sylvain institution
"""
  SYLVAIN
}

```

Tester avec la Query
```GraphQL
{
  getFilms(search: "Le Seigneur des anneaux : Les Deux Tours"){
    director{
      films(search: "Le Seigneur des anneaux"){
        title
      }
    }
  }
}


```

<details>
  <summary>Informations</summary>

L'annontation `@Argument` fonctionne de la même facon pour un resolver que pour une query.


</details>

<details>
  <summary>Réponse</summary>

#### **`src/main/java/com/soprasteria/graphqllab/FilmQuery.java`**
```java

//director resolver

@SchemaMapping(typeName = "Director", field = "films")
public List<Film> getFilmsOfDirector(@Argument("search") String search, Director director) {
    var films = this.films.stream();
    // search filter
    if (search != null) {
        films = films.filter((film) -> film.getTitle().toLowerCase().contains(search.toLowerCase()));
    }
    return films.filter((film) -> film.getDirectorId() == director.getId()).collect(Collectors.toList());
}
```

[6-ex6](https://gitlab.com/krack/graphql-lab/-/tags/6-ex6)

</details>


## Pour aller plus loin : 
- Géré des acteurs [Résultat](https://gitlab.com/krack/graphql-lab/-/tags/7-actors).
- Gérer le parralélisme des resolver avec CompletableFuture [Résultat](https://gitlab.com/krack/graphql-lab/-/tree/main).
